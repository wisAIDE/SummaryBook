# 1장 헬로 파이썬

## 파이썬
#### 산술 연산
##### 자료형
#### 변수
#### 리스트
#### 딕셔너리
#### bool
#### if문

```python
if conditions:
	#~~~
elif conditions:
	# ~~~
else:
	# ~~~
```
#### for문

```python
for x in ARR:
	# ~~~
for x in range(0, 10):
	# ~~~
```
#### 함수

```python
def function_name(self, params):
	# ~~~
```

##### 클래스

```python
class NAME:
	def __init__(self, params..):

	def funcions(self, params..):
```
## numpy

```python
import numpy as np

arr = np.array([1, 2, 3])
```

## matplotlib

```python
import numpy as np
import matplotlib.pyplot as plt

x = np.arange(0, 6, 0.1)
y = np.sin(x)

plt.plot(x, y)
plt.show()
```

```python
import matplotlib.pyplot as plt
from matplotlib.image import imread

img = imread('lena.png')

plt.imshow(img)
plt.show()
```





